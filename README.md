# easy-sys-go

#### 介绍

easy-sys 是一个通用的后台管理系统，实现了用户、菜单、角色、国际化管理四大模块。
其中菜单模块可以管理系统菜单，并且管理菜单对应的权限。角色管理中可以管理角色拥有哪些权限及菜单，角色拥有哪些用户。国际化管理中实现了系统中所有的国际化的后台管理，省去了用户需要在 json 文件中维护国际化的麻烦。
easy-sys-go 是 easy-sys 后端的 golang 语言的实现。地址[https://gitee.com/fantasylan/easy-sys-go](https://gitee.com/fantasylan/easy-sys-go)
easy-sys-java 是 easy-sys 后端的 Java 语言实现。地址[https://gitee.com/fantasylan/easy-sys-java](https://gitee.com/fantasylan/easy-sys-java)
easy-sys-ui 是 easy-sys 的前端框架项目地址[https://gitee.com/fantasylan/easy-sys-ui](https://gitee.com/fantasylan/easy-sys-ui)

Demo 演示环境地址：[https://www.fantasylan.cn/sys](https://www.fantasylan.cn/sys)
文档地址：[http://easy-sys.fantasylan.cn](http://easy-sys.fantasylan.cn/)

微信号：Fantasy_Lan

#### 软件架构

使用第三方开源实现如下：
github.com/dgrijalva/jwt-go,用于 JWT 鉴权
github.com/gin-contrib/sessions,用户登录后，服务器中状态的保持
github.com/gin-gonic/gin,基础高性能 web 框架
github.com/jinzhu/gorm,golang 与数据库交互的 ORM 库
github.com/sirupsen/logrus,日志记录中间件
golang.org/x/crypto,密码加密使用库，pbkdf2 算法

#### 开发环境安装

1.  golang 下载安装[https://golang.google.cn/dl/](https://golang.google.cn/dl/)
2.  vscode 下载安装[https://code.visualstudio.com/Download](https://code.visualstudio.com/Download)
3.  vscode 配置 golang 开发环境[https://blog.csdn.net/m0_37684037/article/details/93173560](https://blog.csdn.net/m0_37684037/article/details/93173560)
4.  使用 vscode 打开 easy-sys-go 的代码文件夹

#### 快速开始

1.  下载代码，在 git 命令行使用命令 git clone https://gitee.com/fantasylan/easy-sys-go.git
2.  运行 easy-sys-go，使用 vscode 打开代码文件夹中的 main.go，按 F5，提示执行
3.  运行起动前端项目 easy-sys-ui，浏览器打开[http://localhost:8080](http://localhost:8080)
