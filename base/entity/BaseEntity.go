package entity

// BaseEntity 自定义的简单基础对象
type BaseEntity struct {
	CreateBy   uint64   `json:"createBy"`
	UpdateBy   uint64   `json:"updateBy"`
	CreateTime JSONTime `json:"createTime"`
	UpdateTime JSONTime `json:"updateTime"`
}
