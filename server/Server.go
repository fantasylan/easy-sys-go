package server

import (
	"easy-sys/server/middleware/easyjwt"
	"easy-sys/server/middleware/logger"
	"easy-sys/server/middleware/session"
	"easy-sys/server/middleware/exception"
	"easy-sys/server/router"

	"github.com/gin-contrib/sessions/memstore"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func Server() *gin.Engine {
	// Engin
	engine := gin.Default()
	store := memstore.NewStore([]byte("secret"))

	var opt = sessions.Options{}
	opt.HttpOnly = true
	opt.Path = "/"
	opt.MaxAge = 30 * 60 *1000 // 半个小时过期

	store.Options(opt)
	engine.Use(exception.SetUp())
	engine.Use(logger.LogToDb()) // 记录操作日志
	engine.Use(sessions.Sessions("mysession11", store)) // 定义session中间件
	engine.Use(easyjwt.JWTAuth())                       // 定义全局的中间件
	engine.Use(session.UserInfoToContext())


	router.InitRouter(engine)
	return engine
}
