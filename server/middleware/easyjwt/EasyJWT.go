package easyjwt

import (
	"easy-sys/config"
	"easy-sys/utils"
	"errors"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// JWTAuth 中间件，检查token
func JWTAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		if checkAllow(c) {
			// 如果是不需要验证的，直接返回
			return
		}
		token := c.Request.Header.Get("X-Token")
		if token == "" {
			c.JSON(http.StatusOK, utils.GetFailureMessage("请求未携带token，无权限访问"))
			c.Abort()
			return
		}

		// parseToken 解析token包含的信息
		claims, err := ParseToken(token)
		if err != nil {
			if err == TokenExpired {
				c.JSON(http.StatusOK, utils.GetTokenExpiredMessage())
				c.Abort()
				return
			}
			c.JSON(http.StatusOK, utils.GetFailureMessage("解析Token 出错："+err.Error()))
			c.Abort()
			return
		}
		// 继续交由下一个路由处理,并将解析出的信息传递下去
		c.Set("claims", claims)
		c.Set("userId", claims.UserId)
	}
}

func checkAllow(c *gin.Context) bool {
	var path = c.Request.URL.Path // 获取请求路径
	if len(config.Cfg.Sys.NoAuthPathArr) > 0 {
		for _, noAuthPath := range config.Cfg.Sys.NoAuthPathArr {
			if path == noAuthPath {
				return true
			}
		}
	}
	return false
}

// 一些常量
var (
	TokenExpired     error  = errors.New("Token is expired")
	TokenNotValidYet error  = errors.New("Token not active yet")
	TokenMalformed   error  = errors.New("That's not even a token")
	TokenInvalid     error  = errors.New("Couldn't handle this token:")
	SignKey          string = "newtrekWangfdsfsfsfsdfsdfdfsfffhthtrte" //密钥，同java代码
	mySignKeyBytes          = []byte(SignKey)
)

// 载荷，可以加一些自己需要的信息
type CustomClaims struct {
	UserId      uint64 `json:"userId"`
	UserName    string `json:"name"`
	UserAccount string `json:"userAccount"`
	jwt.StandardClaims
}

// CreateToken 生成一个token
func CreateToken(claims CustomClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(mySignKeyBytes)
}

// 解析Tokne
func ParseToken(tokenString string) (*CustomClaims, error) {
	token, err := jwt.ParseWithClaims(tokenString, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return mySignKeyBytes, nil
	})
	if err != nil {
		if ve, ok := err.(*jwt.ValidationError); ok {
			if ve.Errors&jwt.ValidationErrorMalformed != 0 {
				return nil, TokenMalformed
			} else if ve.Errors&jwt.ValidationErrorExpired != 0 {
				// Token is expired
				return nil, TokenExpired
			} else if ve.Errors&jwt.ValidationErrorNotValidYet != 0 {
				return nil, TokenNotValidYet
			} else {
				return nil, TokenInvalid
			}
		}
	}
	if claims, ok := token.Claims.(*CustomClaims); ok && token.Valid {
		return claims, nil
	}
	return nil, TokenInvalid
}
