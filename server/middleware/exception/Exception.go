package exception

import (
	"easy-sys/utils"
	"net/http"
	"runtime/debug"

	"github.com/gin-gonic/gin"
)

// SetUp 设置遇到错误的时候怎么处理，统一发送错误信息
func SetUp() gin.HandlerFunc {

	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {

				DebugStack := string(debug.Stack())
				c.JSON(http.StatusOK, utils.GetFailureMessage(DebugStack))
			}
		}()
		c.Next()
	}
}
