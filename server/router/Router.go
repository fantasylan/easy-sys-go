package router

import (
	sysRouter "easy-sys/sys/router"

	blogRouter "easy-sys/blog/router"

	"github.com/gin-gonic/gin"
)

// InitRouter 初始化系统全部路由
func InitRouter(engine *gin.Engine) {
	sysRouter.InitSysRouter(engine)
	blogRouter.InitBlogRouter(engine)
}
