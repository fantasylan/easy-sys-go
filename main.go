package main

import (
	"easy-sys/config"
	"easy-sys/server"
	"strconv"
)

func main() {
	r := server.Server()
	// Listen and Server in 0.0.0.0:8080
	r.Run(":" + strconv.Itoa(config.Cfg.Sys.Port))

}
