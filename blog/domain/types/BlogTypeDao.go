package types

// type是golang中的关键字，这里加个s做区别

import (
	"easy-sys/base/dao"
	"easy-sys/base/entity"

	"github.com/jinzhu/gorm"
)

// List 返回类型分页列表
func List(blogType BlogType, page entity.EasyPage) entity.EasyPage {
	var types []BlogType
	dao.Db.Where(blogType).Limit(page.PageSize()).Offset(page.PageOffset()).Find(&types)
	page.Records = types
	var total int
	dao.Db.Model(&blogType).Where(blogType).Count(&total)
	page.Total = total
	return page
}

// GetAllTypeList 返回所有类型
func GetAllTypeList(blogType BlogType) []BlogType {
	var types []BlogType
	dao.Db.Where(blogType).Find(&types)
	return types
}

// Save 新建或者修改
func Save(blogType BlogType) int64 {
	var db *gorm.DB
	if blogType.TypeId == 0 {
		db = dao.Db.Create(&blogType)
	} else {
		db = dao.Db.Model(&blogType).Where("type_id = ?", blogType.TypeId).Update(&blogType)
	}
	return db.RowsAffected
}

// Delete 删除类型
func Delete(blogType BlogType) int64 {
	var db *gorm.DB
	if blogType.TypeId != 0 {
		db = dao.Db.Where("type_id = ?", blogType.TypeId).Delete(&blogType)
	}
	return db.RowsAffected
}
