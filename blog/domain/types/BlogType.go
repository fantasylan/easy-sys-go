package types

// type是golang中的关键字，这里加个s做区别

import "easy-sys/base/entity"

// BlogType 博客文章类型
type BlogType struct {
	TypeId   uint64 `json:"typeId"`
	TypeName string `json:"typeName"`
	TypeDesc string `json:"typeDesc"`
	entity.BaseEntity
}

// TableName 返回真正的数据库表名称
func (BlogType) TableName() string {
	return "blog_type"
}
