package article

import "easy-sys/base/entity"

// BlogArticle 博客文章
type BlogArticle struct {
	ArticleId       uint64 `json:"articleId"`
	ArticleTitle    string `json:"articleTitle"`
	ArticleAbstract string `json:"articleAbstract"`
	ArticleContent  string `json:"articleContent"`
	ArticleType     uint64 `json:"articleType"`
	entity.BaseEntity
}

// TableName 返回真正的数据库表名称
func (BlogArticle) TableName() string {
	return "blog_article"
}
