package article

import (
	"easy-sys/base/dao"
	"easy-sys/base/entity"

	"github.com/jinzhu/gorm"
)

// List 返回文章分页列表
func List(blogArticle BlogArticle, page entity.EasyPage) entity.EasyPage {
	var articles []BlogArticle
	dao.Db.Where(blogArticle).Limit(page.PageSize()).Offset(page.PageOffset()).Find(&articles)
	page.Records = articles
	var total int
	dao.Db.Model(&blogArticle).Where(blogArticle).Count(&total)
	page.Total = total
	return page
}

// Save 新建或者修改
func Save(blogArticle BlogArticle) int64 {
	var db *gorm.DB
	if blogArticle.ArticleId == 0 {
		db = dao.Db.Create(&blogArticle)
	} else {
		db = dao.Db.Model(&blogArticle).Where("article_id = ?", blogArticle.ArticleId).Update(&blogArticle)
	}
	return db.RowsAffected
}

// Delete 删除文章
func Delete(blogArticle BlogArticle) int64 {
	var db *gorm.DB
	if blogArticle.ArticleId != 0 {
		db = dao.Db.Where("article_id = ?", blogArticle.ArticleId).Delete(&blogArticle)
	}
	return db.RowsAffected
}
