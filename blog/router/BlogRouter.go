package router

import (
	"easy-sys/blog/router/article"
	"easy-sys/blog/router/types"

	"github.com/gin-gonic/gin"
)

// InitBlogRouter 初始化Blog路由
func InitBlogRouter(engine *gin.Engine) *gin.RouterGroup {
	blogRouter := engine.Group("/blog")
	// 指定地址和端口号
	article.Routes(blogRouter)
	types.Routes(blogRouter)
	return blogRouter
}
