package types

// type是golang中的关键字，这里加个s做区别

import (
	"easy-sys/blog/service/types"
	"easy-sys/server/middleware/permission"

	"github.com/gin-gonic/gin"
)

// Routes 国际化相关接口初始化
func Routes(route *gin.RouterGroup) {
	typeRouters := route.Group("/type")
	typeRouters.POST("list", permission.RouterHasPermission("blog.type.list"), types.List)
	typeRouters.POST("getAllTypeList", types.GetAllTypeList) // 公开接口，不需要权限
	typeRouters.POST("save", permission.RouterHasPermission("blog.type.save"), types.Save)
	typeRouters.POST("delete", permission.RouterHasPermission("blog.type.delete"), types.Delete)
}
