package article

import (
	"easy-sys/blog/service/article"
	"easy-sys/server/middleware/permission"

	"github.com/gin-gonic/gin"
)

// Routes 国际化相关接口初始化
func Routes(route *gin.RouterGroup) {
	articleRoutes := route.Group("/article")
	articleRoutes.POST("list", article.List) // 公开接口，不需要权限
	articleRoutes.POST("save", permission.RouterHasPermission("blog.article.save"), article.Save)
	articleRoutes.POST("delete", permission.RouterHasPermission("blog.article.delete"), article.Delete)
}
