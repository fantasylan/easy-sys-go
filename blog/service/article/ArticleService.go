package article

import (
	"easy-sys/base/entity"
	"easy-sys/blog/domain/article"
	"easy-sys/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// List 获取国际化分页列表
func List(context *gin.Context) {
	var blogArticle article.BlogArticle
	var pageVO entity.EasyPage
	context.ShouldBindQuery(&pageVO)
	context.ShouldBind(&blogArticle)

	pageVO = article.List(blogArticle, pageVO)

	context.JSON(http.StatusOK, utils.GetSuccessMessage(pageVO))
}

// Save 新建或者修改国际化
func Save(c *gin.Context) {
	var blogArticle article.BlogArticle
	c.ShouldBind(&blogArticle)
	if blogArticle.ArticleId == 0 {
		blogArticle.CreateTime = entity.JSONTime(time.Now())
		blogArticle.CreateBy = c.MustGet("userId").(uint64)
	}
	// 无论创建还是修改都是需要更新修改时间和修改人的
	blogArticle.UpdateTime = entity.JSONTime(time.Now())
	blogArticle.UpdateBy = c.MustGet("userId").(uint64)
	var updateCount = article.Save(blogArticle)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("保存失败"))
	}
}

// Delete 删除菜单
func Delete(c *gin.Context) {
	var blogArticle article.BlogArticle
	c.ShouldBind(&blogArticle)
	var updateCount = article.Delete(blogArticle)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("删除失败"))
	}
}
