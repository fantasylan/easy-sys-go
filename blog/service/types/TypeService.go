package types

// type是golang中的关键字，这里加个s做区别

import (
	"easy-sys/base/entity"
	"easy-sys/blog/domain/types"
	"easy-sys/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// List 获取类型分页列表
func List(context *gin.Context) {
	var blogType types.BlogType
	var pageVO entity.EasyPage
	context.ShouldBindQuery(&pageVO)
	context.ShouldBind(&blogType)

	pageVO = types.List(blogType, pageVO)

	context.JSON(http.StatusOK, utils.GetSuccessMessage(pageVO))
}

// GetAllTypeList 获取所有的类型
func GetAllTypeList(context *gin.Context) {
	var blogType types.BlogType
	context.ShouldBind(&blogType)

	var types = types.GetAllTypeList(blogType)

	context.JSON(http.StatusOK, utils.GetSuccessMessage(types))
}

// Save 新建或者修改类型
func Save(c *gin.Context) {
	var blogType types.BlogType
	c.ShouldBind(&blogType)
	if blogType.TypeId == 0 {
		blogType.CreateTime = entity.JSONTime(time.Now())
		blogType.CreateBy = c.MustGet("userId").(uint64)
	}
	// 无论创建还是修改都是需要更新修改时间和修改人的
	blogType.UpdateTime = entity.JSONTime(time.Now())
	blogType.UpdateBy = c.MustGet("userId").(uint64)
	var updateCount = types.Save(blogType)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("保存失败"))
	}
}

// Delete 删除类型
func Delete(c *gin.Context) {
	var blogType types.BlogType
	c.ShouldBind(&blogType)
	var updateCount = types.Delete(blogType)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("删除失败"))
	}
}
