package utils

import (
	"easy-sys/base/entity"
	"encoding/json"
	"fmt"
)

func GetSuccessMessageJson(data interface{}) string {
	//struct 到json str
	if b, err := json.Marshal(data); err == nil {
		return string(b)
	} else {
		fmt.Println("================struct 到json str==出错了")
		return "struct 到json str==出错了"
	}

}

func GetSuccessMessage(data interface{}) interface{} {
	var data1 = entity.EasyMessage{}
	data1.Code = 20000
	data1.Data = data
	return data1
}

func GetFailureMessage(data string) interface{} {
	var data1 = entity.EasyMessage{}
	data1.Code = 50000
	data1.Message = data
	return data1
}

// GetTokenExpiredMessage 获取token过期的返回结果
func GetTokenExpiredMessage() interface{} {
	var data1 = entity.EasyMessage{}
	data1.Code = 50014
	data1.Message = "Token 已过期"
	return data1
}
