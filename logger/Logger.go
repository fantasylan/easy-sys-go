package logger

import (
	"easy-sys/config"
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
)

func Log(message ...interface{}) {
	fmt.Print(message)
}

var logger = logrus.New()

func init() {
	logFilePath := config.Cfg.Log.FilePath
	f, _ := os.Create(logFilePath)
	logger.Out = f
	if config.Cfg.Log.LogLevel == "" {
		config.Cfg.Log.LogLevel = "info"
	}
	if config.Cfg.Log.LogLevel == "info" {
		logger.Level = logrus.InfoLevel
	}
	if config.Cfg.Log.LogLevel == "debug" {
		logger.Level = logrus.DebugLevel
	}
	//设置日志格式
	logger.SetFormatter(&logrus.TextFormatter{})
}

// 日志记录到文件
func Debug(message ...interface{}) {
	logger.Debug(message)
}

func Info(message ...interface{}) {
	logger.Info(message)
}

func Warn(message ...interface{}) {
	logger.Warn(message)
}

func Error(message ...interface{}) {
	logger.Error(message)
}