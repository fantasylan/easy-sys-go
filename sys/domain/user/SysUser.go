package user

import (
	"easy-sys/base/entity"
	"easy-sys/sys/domain/menu"
)

type SysUser struct {
	UserId       uint64         `json:"userId"`
	UserName     string         `json:"userName"`
	UserPwd      string         `json:"userPwd"`
	UserSalt     string         `json:"userSalt"`
	UserEmail     string         `json:"userEmail"`
	UserMobile   string         `json:"userMobile"`
	UserStatus   string         `json:"userStatus"`
	Introduction string         `gorm:"-" json:"introduction"`
	Avatar       string         `gorm:"-" json:"avatar"`
	Name         string         `gorm:"-" json:"name"`
	RoleIdList   []uint64       `gorm:"-" json:"roleIdList"`
	MenuList     []menu.SysMenu `gorm:"-" json:"menuList"`
	Roles        []string       `gorm:"-" json:"roles"`
	Permissions  []string       `gorm:"-" json:"permissions"`
	entity.BaseEntity
}

// 设置User的表名为`profiles`
func (SysUser) TableName() string {
	return "sys_user"
}
