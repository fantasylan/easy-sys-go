package i18n

import (
	"easy-sys/base/dao"
	"easy-sys/base/entity"

	"github.com/jinzhu/gorm"
)

// List 返回国际化的分页列表
func List(sysI18n SysI18n, page entity.EasyPage) entity.EasyPage {
	var i18ns []SysI18n
	dao.Db.Where(sysI18n).Limit(page.PageSize()).Offset(page.PageOffset()).Find(&i18ns)
	page.Records = i18ns
	var total int
	dao.Db.Model(&sysI18n).Where(sysI18n).Count(&total)
	page.Total = total
	return page
}

// Save 新建或者修改国际化
func Save(sysI18n SysI18n) int64 {
	var db *gorm.DB
	if sysI18n.I18nId == 0 {
		db = dao.Db.Create(&sysI18n)
	} else {
		db = dao.Db.Model(&sysI18n).Where("i18n_id = ?", sysI18n.I18nId).Update(&sysI18n)
	}
	return db.RowsAffected
}

// GetAllI18nByLang 获取lang 语种下的所有国际化
func GetAllI18nByLang(lang string) []SysI18n {
	var i18ns []SysI18n
	dao.Db.Where("I18n_Lang = ?", lang).Find(&i18ns)
	return i18ns
}

func Delete(sysI18n SysI18n) int64 {
	var db *gorm.DB
	if sysI18n.I18nId != 0 {
		db = dao.Db.Where("i18n_id = ?", sysI18n.I18nId).Delete(&sysI18n)
	}
	return db.RowsAffected
}
