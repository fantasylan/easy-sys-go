package i18n

import "easy-sys/base/entity"

// SysI18n 系统国际化
type SysI18n struct {
	I18nId      uint64 `json:"i18nId"`
	I18nKey     string `json:"i18nKey"`
	I18nContent string `json:"i18nContent"`
	I18nLang    string `json:"i18nLang"`
	I18nRmk     string `json:"i18nRmk"`
	entity.BaseEntity
}

// TableName 返回真正的数据库表名称
func (SysI18n) TableName() string {
	return "sys_i18n"
}
