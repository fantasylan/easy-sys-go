package log

import (
	"easy-sys/base/entity"
)

type SysLog struct {
	LogId      uint64          `json:"logId"`
	LogStart   entity.JSONTime `json:"logStart"`
	LogEnd     entity.JSONTime `json:"logEnd"`
	LogLatency uint64          `json:"logLatency"`
	LogUri     string          `json:"logUri"`
	LogStatus  int             `json:"logStatus"`
	LogIp      string          `json:"logIp"`
	LogUserId  uint64          `json:"logUserId"`
	LogMethod  string          `json:"logMethod"`
	entity.BaseEntity
}

// 设置User的表名为`profiles`
func (SysLog) TableName() string {
	return "sys_log"
}
