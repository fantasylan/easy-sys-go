package log

import (
	"easy-sys/base/dao"
	"easy-sys/base/entity"
	"github.com/jinzhu/gorm"
)

func List(sysLog SysLog, page entity.EasyPage) entity.EasyPage {
	var logs []SysLog
	dao.Db.Where(sysLog).Limit(page.PageSize()).Offset(page.PageOffset()).Find(&logs)
	page.Records = logs
	var total int
	dao.Db.Model(&SysLog{}).Where(sysLog).Count(&total)
	page.Total = total
	return page
}

func Save(sysLog SysLog) int64 {
	var db *gorm.DB
	if sysLog.LogId == 0 {
		db = dao.Db.Create(&sysLog)
	} else {
		db = dao.Db.Model(&sysLog).Where("log_id = ?", sysLog.LogId).Update(&sysLog)
	}
	return db.RowsAffected
}

func GetAllI18nByLang(lang string) []SysLog {
	var i18ns []SysLog
	dao.Db.Where("I18n_Lang = ?", lang).Find(&i18ns)
	return i18ns
}
