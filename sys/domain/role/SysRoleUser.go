package role

type SysRoleUser struct {
	RoleId uint64
	UserId uint64
}

// 设置User的表名为`profiles`
func (SysRoleUser) TableName() string {
	return "sys_user_role"
}
