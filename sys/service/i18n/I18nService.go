package i18n

import (
	"easy-sys/base/entity"
	"easy-sys/sys/domain/i18n"
	"easy-sys/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// List 获取国际化分页列表
func List(context *gin.Context) {
	var sysI18n i18n.SysI18n
	var pageVO entity.EasyPage
	context.ShouldBindQuery(&pageVO)
	context.ShouldBind(&sysI18n)

	pageVO = i18n.List(sysI18n, pageVO)

	context.JSON(http.StatusOK, utils.GetSuccessMessage(pageVO))
}

// Save 新建或者修改国际化
func Save(c *gin.Context) {
	var sysI18n i18n.SysI18n
	c.ShouldBind(&sysI18n)
	if sysI18n.I18nId == 0 {
		sysI18n.CreateTime = entity.JSONTime(time.Now())
		sysI18n.CreateBy = c.MustGet("userId").(uint64)
	}
	// 无论创建还是修改都是需要更新修改时间和修改人的
	sysI18n.UpdateTime = entity.JSONTime(time.Now())
	sysI18n.UpdateBy = c.MustGet("userId").(uint64)
	var updateCount = i18n.Save(sysI18n)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("保存失败"))
	}
}

// Delete 删除菜单
func Delete(c *gin.Context) {
	var sysI18n i18n.SysI18n
	c.ShouldBind(&sysI18n)
	var updateCount = i18n.Delete(sysI18n)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("删除失败"))
	}
}
