package menu

import (
	"easy-sys/base/entity"
	"easy-sys/sys/domain/menu"
	"easy-sys/sys/domain/role"
	"easy-sys/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func Save(c *gin.Context) {
	var sysMenu menu.SysMenu
	c.ShouldBind(&sysMenu)
	if sysMenu.MenuId == 0 {
		sysMenu.CreateTime = entity.JSONTime(time.Now())
		sysMenu.CreateBy = c.MustGet("userId").(uint64)
	}
	// 无论创建还是修改都是需要更新修改时间和修改人的
	sysMenu.UpdateTime = entity.JSONTime(time.Now())
	sysMenu.UpdateBy = c.MustGet("userId").(uint64)
	var updateCount = menu.Save(sysMenu)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("保存失败"))
	}

}

func List(c *gin.Context) {
	var sysMenu menu.SysMenu
	c.ShouldBind(&sysMenu)

	var pageVO entity.EasyPage
	c.ShouldBindQuery(&pageVO)

	c.JSON(http.StatusOK, utils.GetSuccessMessage(menu.List(sysMenu, pageVO)))
	return
}

func All(c *gin.Context) {
	c.JSON(http.StatusOK, utils.GetSuccessMessage(menu.All()))
	return
}

func GetRoleMenu(c *gin.Context) {
	var sysRole role.SysRole
	c.ShouldBind(&sysRole)
	c.JSON(http.StatusOK, utils.GetSuccessMessage(menu.GetRoleMenuByRoleId(sysRole.RoleId)))
	return
}

// Delete 删除菜单
func Delete(c *gin.Context) {
	var sysMenu menu.SysMenu
	c.ShouldBind(&sysMenu)
	var updateCount = menu.Delete(sysMenu)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("删除失败"))
	}
}
