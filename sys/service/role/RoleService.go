package user

import (
	"easy-sys/base/entity"
	"easy-sys/sys/domain/role"
	"easy-sys/utils"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

// List 获取角色分页
func List(context *gin.Context) {
	var roleVO role.SysRole
	var pageVO entity.EasyPage
	context.ShouldBindQuery(&pageVO)
	context.ShouldBind(&roleVO)

	pageVO = role.List(roleVO, pageVO)

	context.JSON(http.StatusOK, utils.GetSuccessMessage(pageVO))
}

// Delete 删除角色
func Delete(c *gin.Context) {
	var sysRole role.SysRole
	c.ShouldBind(&sysRole)
	var updateCount = role.Delete(sysRole)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("删除失败"))
	}
}

// Save 新增或者修改角色
func Save(c *gin.Context) {
	var sysRole role.SysRole
	c.ShouldBind(&sysRole)
	if sysRole.RoleId == 0 {
		sysRole.CreateTime = entity.JSONTime(time.Now())
		sysRole.CreateBy = c.MustGet("userId").(uint64)
	}
	// 无论创建还是修改都是需要更新修改时间和修改人的
	sysRole.UpdateTime = entity.JSONTime(time.Now())
	sysRole.UpdateBy = c.MustGet("userId").(uint64)
	var updateCount = role.Save(sysRole)
	if updateCount == 1 {
		c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
	} else {
		c.JSON(http.StatusOK, utils.GetFailureMessage("保存失败"))
	}
}

// SaveRoleAuth 保存角色权限
func SaveRoleAuth(context *gin.Context) {
	var roleVO role.SysRole
	context.ShouldBind(&roleVO)
	role.SaveRoleAuth(roleVO)

	context.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
}

// AddRoleUser 添加角色对应用户
func AddRoleUser(c *gin.Context) {
	var roleVO role.SysRole
	c.ShouldBind(&roleVO)
	role.InsertRoleUserByRole(roleVO)

	c.JSON(http.StatusOK, utils.GetSuccessMessage("保存成功"))
}

// DeleteRoleUserByRole 删除角色对应的用户
func DeleteRoleUserByRole(c *gin.Context) {
	var roleVO role.SysRole
	c.ShouldBind(&roleVO)
	role.DeleteRoleUserByRole(roleVO)

	c.JSON(http.StatusOK, utils.GetSuccessMessage("删除成功"))
}
