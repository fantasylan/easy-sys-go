package menu

import (
	"easy-sys/server/middleware/permission"
	"easy-sys/sys/service/menu"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.RouterGroup) {
	menuRoutes := route.Group("/menu")
	menuRoutes.POST("all", permission.RouterHasPermission("sys.menu.all"), menu.All)
	menuRoutes.POST("getRoleMenu", permission.RouterHasPermission("sys.menu.getRoleMenu"), menu.GetRoleMenu)
	menuRoutes.POST("list", permission.RouterHasPermission("sys.menu.list"), menu.List)
	menuRoutes.POST("save", permission.RouterHasPermission("sys.menu.save"), menu.Save)
	menuRoutes.POST("delete", permission.RouterHasPermission("sys.menu.delete"), menu.Delete)
}
