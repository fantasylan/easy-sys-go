package i18n

import (
	"easy-sys/server/middleware/permission"
	"easy-sys/sys/service/i18n"

	"github.com/gin-gonic/gin"
)

// Routes 国际化相关接口初始化
func Routes(route *gin.RouterGroup) {
	i18nRoutes := route.Group("/i18n")
	i18nRoutes.POST("list", permission.RouterHasPermission("sys.i18n.list"), i18n.List)
	i18nRoutes.POST("save", permission.RouterHasPermission("sys.i18n.save"), i18n.Save)
	i18nRoutes.POST("delete", permission.RouterHasPermission("sys.i18n.delete"), i18n.Delete)
}
