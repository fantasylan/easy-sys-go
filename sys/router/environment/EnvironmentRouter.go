package environment

import (
	"easy-sys/sys/service/environment"

	"github.com/gin-gonic/gin"
)

func Routes(route *gin.RouterGroup) {
	envRoutes := route.Group("/env")
	envRoutes.GET("/info", environment.GetEnvInfo)
}
