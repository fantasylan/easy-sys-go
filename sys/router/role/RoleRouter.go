package role

import (
	"easy-sys/server/middleware/permission"
	roleService "easy-sys/sys/service/role"

	"github.com/gin-gonic/gin"
)

// Routes 角色路由初始化
func Routes(route *gin.RouterGroup) {
	roleRoutes := route.Group("/role")
	roleRoutes.POST("list", permission.RouterHasPermission("sys.role.list"), roleService.List)
	roleRoutes.POST("saveRoleAuth", permission.RouterHasPermission("sys.role.saveRoleAuth"), roleService.SaveRoleAuth)
	roleRoutes.POST("addRoleUser", permission.RouterHasPermission("sys.role.addRoleUser"), roleService.AddRoleUser)
	roleRoutes.POST("deleteRoleUserByRole", permission.RouterHasPermission("sys.role.deleteRoleUserByRole"), roleService.DeleteRoleUserByRole)
	roleRoutes.POST("delete", permission.RouterHasPermission("sys.role.delete"), roleService.Delete)
	roleRoutes.POST("save", permission.RouterHasPermission("sys.role.save"), roleService.Save)
}
