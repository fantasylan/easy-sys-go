package user

import (
	"easy-sys/server/middleware/permission"
	"easy-sys/sys/service/user"

	"github.com/gin-gonic/gin"
)

// Routes 用户路由.
func Routes(route *gin.RouterGroup) {
	userRoutes := route.Group("/user")
	userRoutes.POST("login", user.Login)
	userRoutes.GET("info", user.Info)
	userRoutes.POST("getUserByRole", permission.RouterHasPermission("sys.user.getUserByRole"), user.GetUserByRole)
	userRoutes.POST("list", permission.RouterHasPermission("sys.user.list"), user.List)
	userRoutes.POST("save", permission.RouterHasPermission("sys.user.save"), user.Save)
	userRoutes.POST("delete", permission.RouterHasPermission("sys.user.delete"), user.Delete)
	userRoutes.POST("logout", user.Logout)
}
