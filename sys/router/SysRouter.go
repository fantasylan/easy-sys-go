package sys

import (
	"easy-sys/sys/router/environment"
	"easy-sys/sys/router/i18n"
	"easy-sys/sys/router/role"
	"easy-sys/sys/router/user"

	"easy-sys/sys/router/menu"

	"github.com/gin-gonic/gin"
)

// InitSysRouter 初始化Sys路由
func InitSysRouter(engine *gin.Engine) *gin.RouterGroup {
	sysRouter := engine.Group("/sys")
	// 指定地址和端口号
	user.Routes(sysRouter)
	role.Routes(sysRouter)
	menu.Routes(sysRouter)
	i18n.Routes(sysRouter)
	environment.Routes(sysRouter)
	return sysRouter
}
