package config

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

// Db 数据库配置参数
type Db struct {
	Url          string
	User         string
	Password     string
	Type         string
	MaxIdleConns int
	MaxOpenConns int
	LogMode      bool
}

// SysConfig 系统配置参数，NoAuth是不需要登录拦截的路径，NoAuthPathArr根据NoAuthPath切割出来的不需要登录的路径，Port系统启动端口
type SysConfig struct {
	NoAuthPath          string
	NoAuthPathArr       []string
	Port                int
	Demo                bool   // 是否是演示环境
	DemoSuperUser       string // 演示环境下的超级用户
	DemoNotAllowAuth    string // 演示环境不允许通过的权限（包含其中的关键字符就不能通过）
	DemoNotAllowAuthArr []string
}

// LogConfig 日志文件配置LogLevel日志级别debug，FilePath 日志文件路径
type LogConfig struct {
	LogLevel string
	FilePath string
}

// Configuration 配置结构体
type Configuration struct {
	Db  Db
	Sys SysConfig
	Log LogConfig
}

// Cfg 定义一个cfg的变量待会初始化使用
var Cfg Configuration

// init 方法將會在被引用的時候自動執行
func init() {
	file, _ := os.Open("config/config.json")
	defer file.Close()
	decoder := json.NewDecoder(file)
	conf := Configuration{}
	err := decoder.Decode(&conf)
	if err != nil {
		fmt.Println("Error:", err)
	}
	if conf.Sys.NoAuthPath != "" {
		conf.Sys.NoAuthPathArr = strings.Split(conf.Sys.NoAuthPath, ",")
	}
	if conf.Sys.DemoNotAllowAuth != "" {
		conf.Sys.DemoNotAllowAuthArr = strings.Split(conf.Sys.DemoNotAllowAuth, ",")
	}
	fmt.Println("config 的 init方法执行")
	Cfg = conf // 赋值给全局变量
}
